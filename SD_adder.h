#include "systemc.h"
#include "SD_cell.h"
#include "common.h"

SC_MODULE(SD_adder) {
	sc_in<bool>  a_plus[length];
	sc_in<bool>  a_minus[length];
	sc_in<bool>  B[length];
	// plus minus weg fuer Memristtor Logik
	// ueber Konverter mit 2 eingaengen in+ und in- 
	// und als ausgabe den sd wert... siehe table 1
	sc_out<bool> s_plus[length];
	sc_out<bool> s_minus[length];
	//jeder nen eigenen Konverter, mit Schleife dann drueberlaufen.
	sc_in<bool>  add_sub;
	sc_in<bool>  c_plus_in;
	sc_in<bool>  zero;
	sc_out<bool> c_plus_out;
	 
	sc_signal<bool> c_plus_sig[length-1];
	
	SD_cell *SD_cell_array[length];
	
	int name_length;

	SC_CTOR(SD_adder) {
	   name_length = (int) ceil(log10(length)) + 6; //cell_ + Nummer + \0
	   char name[name_length];

	   for(int i = 0; i < length; i++){
	      sprintf(name, "cell_%d", i);
	      SD_cell_array[i] = new SD_cell(name);
	      
	      if(i == 0) {
		 SD_cell_array[i]->c_plus_in(c_plus_in);
		} 
		else {
		  SD_cell_array[i]->c_plus_in(c_plus_sig[i-1]);
		}
		
		if(i == length - 1) {
		   SD_cell_array[i]->c_plus_out(c_plus_out);
		}
		else {
		   SD_cell_array[i]->c_plus_out(c_plus_sig[i]);
		}

		SD_cell_array[i]->a_plus(a_plus[i]); 
		SD_cell_array[i]->a_minus(a_minus[i]); 
		SD_cell_array[i]->add_sub(add_sub);
		SD_cell_array[i]->zero(zero);
		SD_cell_array[i]->B(B[i]); 
		SD_cell_array[i]->s_plus(s_plus[i]);
		SD_cell_array[i]->s_minus(s_minus[i]);

	    } ;
	 } ;
} ;
