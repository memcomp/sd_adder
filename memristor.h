#include "systemc.h"
#include "common.h"

SC_MODULE(memristor){
	sc_in<sd_digit>		sd_digit_in;
	sc_out<sd_digit>	sd_digit_out;
	//sc_in<bool>			clock;

	sd_digit myValue;

	void read();
	void write();

	SC_CTOR(memristor){
		SC_METHOD(read);
		sensitive << sd_digit_in; //clock;

		SC_METHOD(write);
		sensitive << sd_digit_in; //clock;
	};

};
