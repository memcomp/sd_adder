#include "systemc.h"
#include "memristor_register.h"
#include "common.h"

SC_MODULE(memristor_regfile) {
	sc_in<int> 	addr;
	sc_out<memristor_register> register_out;

	memristor_register register[register_length];
	
	void get();

	SC_CTOR(memristor_regfile) {
		SC_METHOD(get);
		sensitive << addr;
	};
};
