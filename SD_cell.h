#include "systemc.h"
/*
#include "E/B-switch_in.h"
#include "E/B-switch_out.h"
*/
#include "E-B-switch.h"
#include "addstep_1.h"
#include "addstep_2.h"

SC_MODULE(SD_cell){
	sc_in<bool>  B;
	sc_in<bool>  c_plus_in;
	sc_in<bool>  zero ;
	sc_in<bool>  add_sub;
	sc_in<bool>  a_plus;	
	sc_in<bool>  a_minus;
	sc_out<bool> s_plus;
	sc_out<bool> s_minus;
	sc_out<bool> c_plus_out;
// sc_in<bool> clock ;
	
	E_B_switch *switch_in ;
	E_B_switch *switch_out ;
	addstep_1  *step_1;
	addstep_2  *step_2;

	void B_and_Zero() ;
	
//	sc_signal<bool> add_sub, a_plus_, a_minus, B, zero;
	sc_signal<bool> B_and_zero ;
	sc_signal<bool> out1_a_plus, out2_a_minus;
	sc_signal<bool> s_minus_in2, s_plus_in1, z_minus ;
	
	SC_CTOR(SD_cell){
/*		switch_in = new E/B-switch_in("switch_in");
/		switch_out = new E/B-switch_out("switch_out");
*/
		//Komponenten der 'Zelle' instanziieren
		switch_in  = new E_B_switch("switch_in");
		switch_out = new E_B_switch("switch_out");		
		step_1     = new addstep_1("step_1");
		step_2     = new addstep_2("step_2");
		
//		zero.write('1');//oder woanders?????
//              Zero ist Eingangssignal, das von außen - z.B. in der Testbench 
//              auf 1 gelegt wird     
//		B_and_zero.write(B && zero); //oder eigenes Modul????????? wurde uebersetzt
//                                             habe AND-Funktion jedoch mit eigener Methode realisiert  
		
		switch_in->ex(add_sub);
		switch_in->in1(a_plus);
		switch_in->in2(a_minus);
		switch_in->out1(out1_a_plus);
		switch_in->out2(out2_a_minus);
		
		step_1->a_plus(out1_a_plus);
		step_1->a_minus(out2_a_minus);
		step_1->B(B_and_zero);
		step_1->c_plus(c_plus_out);
//		step_1->c_plus(c_plus);
		step_1->z_minus(z_minus);
/*		step_1->z_minus(s_minus_in2);
*/		
		step_2->c_plus(c_plus_in);
		step_2->z_minus(z_minus);
		step_2->s_plus(s_plus_in1);
		step_2->s_minus(s_minus_in2);
		
		switch_out->ex(add_sub);
		switch_out->in1(s_plus_in1);
		switch_out->in2(s_minus_in2);
		switch_out->out1(s_plus);
//		switch_out->out2(s_plus);
		switch_out->out2(s_minus);
		
//                B_out = B && "1" ; 

		SC_METHOD(B_and_Zero);
		sensitive << B << zero ;
		
	};
};
