#include "systemc.h"
#include "common.h"


SC_MODULE(stimuli_add_sub){
	sc_out<bool> B[length];
	sc_out<bool> c_plus_in;
	sc_out<bool> zero ;
	sc_out<bool> add_sub;
	sc_out<bool> a_plus[length];	
	sc_out<bool> a_minus[length];
	sc_in<bool>  clock ;
	//sc_in<bool>  s_plus[length];
	//sc_in<bool>  s_minus[length];
	sc_in<sd_digit> sd_out[length];
	sc_in<bool>  c_plus_out;
	
	void write_stimuli() ;
	void read_result() ;
	
	SC_CTOR(stimuli_add_sub){
 	   SC_THREAD(write_stimuli);
 	   sensitive << clock.pos() ;
	   
 	   SC_THREAD(read_result);
 	   sensitive << clock.pos() ;	   
	};
};
