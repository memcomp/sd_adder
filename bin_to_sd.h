#include "systemc.h"
#include "common.h"

SC_MODULE(bin_to_sd){
	sc_in<bool> s_plus_in[length];
	sc_in<bool> s_minus_in[length];

	sc_out<sd_digit> sd_out[length];


	void convert();
	SC_CTOR(bin_to_sd){
		SC_METHOD(convert);
		int j;
		for(j=0; j<length; j++){
			sensitive << s_plus_in[j] << s_minus_in[j];
		}
	};
};
