#include "E-B-switch.h"

void E_B_switch::exchange(){
	if(ex == 1) {
		out1.write(in2);
		out2.write(in1);
	}
	else{
		out1.write(in1);
		out2.write(in2);
	} ;
	
//	cout << "E/B in:" << " Ex: " << ex << "  In1: " << in1 
//	     << "  In2: " << in2 << endl ;
};
