#include "systemc.h"
#include <limits>
#include "stimuli_add_sub.h"

int convert(sc_bv<length> vector) {

   unsigned temp ;
   int      bitpos;
   
   temp = 0 ;
   for(bitpos = length-1; bitpos >= 0; bitpos--) {
      temp *= 2 ;
      if (vector[bitpos] == 0)
         temp = temp  ;
      else temp++ ; 
   }
   return temp ; 
}


void stimuli_add_sub::write_stimuli() {

   int index ;
   int op1, op2, sum ;
   
   sc_bv<length>   A_in ; //sc_bv ist bitvektor
   sc_bv<length>   B_in ;
   sc_bv<length>   Result ;

   zero      = 1 ;
   c_plus_in = 0 ;
   add_sub   = 0 ; 

   for(index = 0; index < length; index++) 
      /* Aufeinanderfolgendes Erzeugen der binaeren Eingabemuster fuer a_minus (stets alle Bits 0) */
      a_minus[index] = 0 ;
       
   for(op1 = 0; op1 < pow(2,length); op1++) 
      for(op2 = 0; op2 < pow(2,length); op2++) {
		  //hier wird wohl implizit konvertiert
         A_in = op1 ;
         B_in = op2 ;
         sum  = op1 + op2 ;

   cout << "A: " << A_in << "       =  " << op1 << "\n" ;
   cout << "B: " << B_in << "       =  " << op2 << "\n" ;
   cout << "A + B = " << sum << "\n" ;

   for (index = 0; index < length; index++) {
      if (A_in[index] == 0)
            a_plus[index] = 0 ;
        else a_plus[index] = 1 ; 

        if (B_in[index] == 0)
            B[index] = 0 ;
        else B[index] = 1 ;    
   }  

   wait() ;   

   /* Ergebnisse, identisch mit Ausgaengen des DUT, ausgeben */
   read_result() ;	   

   } ;
} ;

void stimuli_add_sub::read_result() {

   int index ;
   int res_plus, res_minus ;   
   sc_bv<length> res_plus_sig  ;
   sc_bv<length> res_minus_sig ;


   cout << sc_time_stamp() << ":" << endl;
   //cout << "C+_out S+    S-" << "\n" ;
   /*cout << "   " << c_plus_out.read() << "   " << s_plus[3].read() << s_plus[2].read() << 
	    s_plus[1].read() << s_plus[0].read() << "  " << s_minus[3].read() << s_minus[2].read() << 
	    s_minus[1].read() << s_minus[0].read() << "\n" ;
	     
   for (index = 0; index < length; index++) {
      res_plus_sig[index]  = s_plus[index] ;
      res_minus_sig[index] = s_minus[index] ;      
   } 

   res_plus  = convert(res_plus_sig) + c_plus_out.read()*pow(2,length) ; ;
   res_minus = convert(res_minus_sig) ;	     
   
   cout << "     " << res_plus << "     -  " << res_minus << "   = " 
        << res_plus-res_minus << "\n" ;
	*/
   
   cout << c_plus_out.read() << " " << sd_out[3].read() << sd_out[2].read() << sd_out[1].read() << sd_out[0].read() << "\n";
   cout << "----------------------------------------------------" << endl ;   
}
