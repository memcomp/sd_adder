#include "systemc.h"

#ifndef length
#define length 4
#endif

#ifndef sd_dig
#define sd_dig
enum sd_digit {MINUS_ONE=-1, ZERO=0, ONE=1};
#endif
