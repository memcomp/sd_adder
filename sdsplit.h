#include "systemc.h"
#include "common.h"

SC_MODULE(sdsplit){
    int mylen;

    sc_in<bool> *c_plus_in;
    sc_in<sd_digit> *sd_digit_in;
    sc_out<sd_digit> *sd_plus_out;
    sc_out<sd_digit> *sd_minus_out;
    //sc_in<bool>       clock;

    void split();

    sdsplit(sc_module_name name_, int len = length) : sc_module(name_), mylen(len) {
        c_plus_in = new sc_in<bool>[len];
        sd_digit_in = new sc_in<sd_digit>[len];
        sd_plus_out = new sc_out<sd_digit>[len];
        sd_minus_out = new sc_out<sd_digit>[len];

    };

    SC_CTOR(sdsplit){
        SC_METHOD(split);
        for (int i = 0; i < mylen; i++){
            sensitive << sd_digit_in[i];c_plus_in; //clock;
        }
    };
};
