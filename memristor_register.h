#include "systemc.h"
#include "memristor.h"

SC_MODULE(memristor_register){
	memristor **m;
	int mylen;

	sc_in<sd_digit>	*sd_digit_in;
	sc_out<sd_digit> *sd_digit_out;
	//sc_in<bool>		clock;

	memristor_register(sc_module_name name_, int len = length) : sc_module(name_), mylen(len) {
		sd_digit_in = new sc_in<sd_digit>[len];
		sd_digit_out = new sc_out<sd_digit>[len];

		m = (memristor**)malloc(sizeof(memristor*) * len);

		int i;
		for(i = 0; i < len; i++) {
			m[i] = new memristor("mem"+i);
			m[i]->sd_digit_in(sd_digit_in[i]);
			m[i]->sd_digit_out(sd_digit_out[i]);
			//m[i]->clock(clock);
		}
	};

};
