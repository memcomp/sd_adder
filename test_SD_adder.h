#include "systemc.h"
#include "SD_adder.h"
#include "stimuli_SD_adder.h"

SC_MODULE(test_SD_adder){

        sc_in<bool>  clock ;

	SD_adder *SD_adder_inst;
	stimuli_SD_adder *stimuli_SD_adder_inst;
	
	sc_signal<bool> a_plus[length];
	sc_signal<bool> a_minus[length];
	sc_signal<bool> B[length];
	sc_signal<bool> s_plus[length];
	sc_signal<bool> s_minus[length];
	sc_signal<bool> add_sub;
	sc_signal<bool> c_plus_in;
	sc_signal<bool> zero;
	sc_signal<bool> c_plus_out;
	
	SC_CTOR(test_SD_adder){

	SD_adder_inst = new SD_adder("SD_adder");
	stimuli_SD_adder_inst = new stimuli_SD_adder("stimuli_SD_adder");

	//verdrahten
	for(int i = 0; i < length; i++){
		SD_adder_inst->a_plus[i](a_plus[i]);
		SD_adder_inst->a_minus[i](a_minus[i]);
		SD_adder_inst->B[i](B[i]);
		SD_adder_inst->s_plus[i](s_plus[i]);
		SD_adder_inst->s_minus[i](s_minus[i]);

		stimuli_SD_adder_inst->a_plus[i](a_plus[i]);
		stimuli_SD_adder_inst->a_minus[i](a_minus[i]);
		stimuli_SD_adder_inst->B[i](B[i]);
		stimuli_SD_adder_inst->s_plus[i](s_plus[i]);
		stimuli_SD_adder_inst->s_minus[i](s_minus[i]);
	}
		
	SD_adder_inst->add_sub(add_sub);
	SD_adder_inst->zero(zero);
	SD_adder_inst->c_plus_in(c_plus_in);
	SD_adder_inst->c_plus_out(c_plus_out);
	
	stimuli_SD_adder_inst->add_sub(add_sub);
	stimuli_SD_adder_inst->zero(zero);
	stimuli_SD_adder_inst->c_plus_in(c_plus_in);
	stimuli_SD_adder_inst->c_plus_out(c_plus_out);

	stimuli_SD_adder_inst->clock(clock) ;
	
	};
};
