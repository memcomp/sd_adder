#include "systemc.h"
#include "add_sub.h"
#include "stimuli_add_sub.h"

SC_MODULE(test_add_sub){

    sc_in<bool>  clock ;

	add_sub_module *add_sub_inst;
	stimuli_add_sub *stimuli_add_sub_inst;
	
	sc_signal<bool> a_plus[length];
	sc_signal<bool> a_minus[length];
	sc_signal<bool> B[length];
	//sc_signal<bool> s_plus[length];
	//sc_signal<bool> s_minus[length];
	sc_signal<bool> add_sub;
	sc_signal<bool> c_plus_in;
	sc_signal<bool> zero;
	sc_signal<bool> c_plus_out;
	sc_signal<sd_digit> sd_out[length];

	SC_CTOR(test_add_sub){

	add_sub_inst = new add_sub_module("add_sub");
	stimuli_add_sub_inst = new stimuli_add_sub("stimuli_add_sub");

	//verdrahten
	for(int i = 0; i < length; i++){
		add_sub_inst->a_plus[i](a_plus[i]);
		add_sub_inst->a_minus[i](a_minus[i]);
		add_sub_inst->B[i](B[i]);
		add_sub_inst->sd_out[i](sd_out[i]);
		//add_sub_inst->s_plus[i](s_plus[i]);
		//add_sub_inst->s_minus[i](s_minus[i]);

		//s_plus[i](add_sub_inst->s_plus[i]);
		//s_minus[i](add_sub_inst->s_minus[i]);
		
		stimuli_add_sub_inst->a_plus[i](a_plus[i]);
		stimuli_add_sub_inst->a_minus[i](a_minus[i]);
		stimuli_add_sub_inst->B[i](B[i]);
		stimuli_add_sub_inst->sd_out[i](sd_out[i]);
		//stimuli_add_sub_inst->s_plus[i](s_plus[i]);
		//stimuli_add_sub_inst->s_minus[i](s_minus[i]);
	}
		
	add_sub_inst->add_sub(add_sub);
	add_sub_inst->zero(zero);
	add_sub_inst->c_plus_in(c_plus_in);
	add_sub_inst->c_plus_out(c_plus_out);

	stimuli_add_sub_inst->add_sub(add_sub);
	stimuli_add_sub_inst->zero(zero);
	stimuli_add_sub_inst->c_plus_in(c_plus_in);
	stimuli_add_sub_inst->c_plus_out(c_plus_out);

	stimuli_add_sub_inst->clock(clock) ;
	
	};
};
