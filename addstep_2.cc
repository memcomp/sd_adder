#include "addstep_2.h"

void addstep_2::calc_s_plus(){ 
	//s_i_+ = not(z_i_-) and c_i-1_+
	s_plus = !z_minus && c_plus;
}

void addstep_2::calc_s_minus(){ 
	//s_i_- = not(c_i-1_+) and z_i_-
	s_minus = !c_plus && z_minus;
//	cout << "addstep2_in: c_+ " << c_plus << " z_- " << z_minus << endl ;	
}
