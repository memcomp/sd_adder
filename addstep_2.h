#include "systemc.h"

SC_MODULE(addstep_2){
	sc_in<bool>  c_plus;
	sc_in<bool>  z_minus;
	sc_out<bool> s_minus;
	sc_out<bool> s_plus;

	void calc_s_plus() ;
	void calc_s_minus() ;
		
	SC_CTOR (addstep_2) {
	   SC_METHOD(calc_s_plus);
	   sensitive << z_minus << c_plus ;
	   
	   SC_METHOD(calc_s_minus);
	   sensitive << z_minus << c_plus ;
	} ;
} ;
