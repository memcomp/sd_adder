#include "systemc.h"

SC_MODULE(E_B_switch){
	sc_in<bool>  ex;
	sc_in<bool>  in1;
	sc_in<bool>  in2;
	sc_out<bool> out1;
	sc_out<bool> out2;
	
        void exchange() ;
	
	SC_CTOR (E_B_switch) { 
	     SC_METHOD(exchange);
	     sensitive << ex << in1 << in2 ;
	} ;
} ;
