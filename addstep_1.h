#include "systemc.h"

SC_MODULE(addstep_1){
	sc_in<bool> a_plus;
	sc_in<bool> a_minus;
	sc_in<bool> B;
	sc_out<bool> c_plus;
	sc_out<bool> z_minus;
	
	void calc_c_plus() ;
	void calc_z_minus() ;
		
	SC_CTOR (addstep_1) {
	   SC_METHOD(calc_c_plus);
	   sensitive << a_plus << B << a_minus ;
	   
	   SC_METHOD(calc_z_minus);
	   sensitive << a_plus << a_minus << B ;
	} ;
} ;
//sensitive_pos gibts auch...
