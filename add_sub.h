#include "systemc.h"
#include "common.h"
#include "SD_adder.h"
#include "memristor_register.h"
#include "bin_to_sd.h"

SC_MODULE(add_sub_module) {
	//SD Adder
	sc_in<bool>  a_plus[length];
    sc_in<bool>  a_minus[length];
    sc_in<bool>  B[length];
    sc_in<bool>  add_sub;
    sc_in<bool>  c_plus_in;
    sc_in<bool>  zero;

    sc_out<bool> c_plus_out;

	sc_signal<bool> s_plus[length];
	sc_signal<bool> s_minus[length];

	sc_signal<sd_digit> sd_out_sig[length];
	sc_out<sd_digit> sd_out[length];

	SD_adder *sd_adder_inst;
	bin_to_sd *bin_to_sd_inst;
	memristor_register *mem_reg;

	SC_CTOR(add_sub_module) {
		sd_adder_inst = new SD_adder("SD_adder");
		bin_to_sd_inst = new bin_to_sd("bin_to_sd");
		mem_reg = new memristor_register("memristor_register");

		int i;
		for(i = 0; i < length; i++) {
			sd_adder_inst->a_plus[i](a_plus[i]);
			sd_adder_inst->a_minus[i](a_minus[i]);
			sd_adder_inst->B[i](B[i]);
			sd_adder_inst->s_plus[i](s_plus[i]);
			sd_adder_inst->s_minus[i](s_minus[i]);

			bin_to_sd_inst->s_plus_in[i](s_plus[i]);
			bin_to_sd_inst->s_minus_in[i](s_minus[i]);
			bin_to_sd_inst->sd_out[i](sd_out_sig[i]);

			mem_reg->sd_digit_in[i](sd_out_sig[i]);
			mem_reg->sd_digit_out[i](sd_out[i]);
		}

		sd_adder_inst->c_plus_out(c_plus_out);
		sd_adder_inst->add_sub(add_sub);
		sd_adder_inst->c_plus_in(c_plus_in);
		sd_adder_inst->zero(zero);
	};
};
