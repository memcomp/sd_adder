#include "systemc.h"
#include "SD_register_array.h"

void SD_register_array::read() {

   
   sd_word_out = SD_reg_array[adr] ;

} //;

void SD_register_array::write() {

   sd_word temp ; 
   int lv ;

   for (lv = 0; lv < length; lv++)
      temp[lv] = sd_word_in.read()[lv] ;
      
   sd_word_out = temp ;   
} //;
