#include "systemc.h"
#include "SD_cell.h"
#include "common.h"

sd_digit digits_1[length];
sd_digit digits_2[length];
sd_digit result[length];




void adder3(){
    // result of step 1
    sd_digit t[length];
    sd_digit z[length];
    // result of step 2
    sd_digit t2[length];
    sd_digit z2[length];
    //step 1
    for(int i = 0; i < length; i++){
        // we interpret the digits differently here -2 -1 0 becomes -1 0 1 in the result
        if( i == 0){
#define GEN_STEP1_FIRST(sd2, res1, res2) \
    if(digits_2[0] == sd2){       \
        t[0] = res1;        \
        z[0] = res2;        \
        continue;       \
    }
            GEN_STEP1_FIRST(ONE, ONE, ZERO)
            GEN_STEP1_FIRST(ZERO, ZERO, ONE)
            GEN_STEP1_FIRST(MINUS_ONE, ZERO, ONE)
#undef GEN_STEP1_FIRST
        }
#define GEN_STEP1(sd1, sd2, res1, res2) \
    if((digits_1[i] == sd1 && digits_2[i] == sd2) \
    || (digits_1[i] == sd2 && digits_2[i] == sd1)){       \
        t[i] = res1;        \
        z[i] = res2;        \
        continue;       \
    }
        GEN_STEP1(ONE, ONE, ONE, ONE)
        GEN_STEP1(ONE, ZERO, ONE, ZERO)
        GEN_STEP1(ONE, MINUS_ONE, ZERO, ONE)

        GEN_STEP1(ZERO, ONE, ONE, ZERO)
        GEN_STEP1(ZERO, ZERO, ZERO, ONE)

        GEN_STEP1(MINUS_ONE, ONE, ZERO, ONE)
#undef GEN_STEP1
    }


    //step 2
    for(int i = 0; i < length; i++){
        // we interpret the digits differently here -1 0 1 means -2 -1 0
        if(i == 0){
#define GEN_STEP2_FIRST(zval, res1, res2) \
    if(z[0] == zval){       \
        t2[0] = res1;        \
        z2[0] = res2;        \
        continue;       \
    }
            GEN_STEP2_FIRST(MINUS_ONE, MINUS_ONE, ZERO)
            GEN_STEP2_FIRST(ZERO, MINUS_ONE, ONE)
            GEN_STEP2_FIRST(ONE, ZERO, ZERO)
#undef GEN_STEP2_FIRST

        }
#define GEN_STEP2(tval, zval, res1, res2) \
    if((t[i] == tval && z[i] == zval)){       \
        t2[i] = res1;        \
        z2[i] = res2;        \
        continue;       \
    }
        GEN_STEP2(ONE, MINUS_ONE, MINUS_ONE, ONE)
        GEN_STEP2(ONE, ZERO, ZERO, ZERO)
        GEN_STEP2(ONE, ONE, ZERO, ONE)

        GEN_STEP2(ZERO, MINUS_ONE, MINUS_ONE, ZERO)
        GEN_STEP2(ZERO, ZERO, MINUS_ONE, ONE)
        GEN_STEP2(ZERO, ONE, ZERO, ZERO)
#undef GEN_STEP2
    }

    //step 3
    for(int i = 0 ; i < length; i++){
        if(i == 0){
            result[0] == ONE;
        }else{
#define GEN_STEP3(tval, zval, res) \
    if((t2[i] == tval && z2[i] == zval)){       \
        result[i] == res;           \
        continue;       \
    }
    GEN_STEP3(ZERO, ZERO, ZERO)
    GEN_STEP3(ZERO, ONE, ONE)
    GEN_STEP3(MINUS_ONE, ZERO, MINUS_ONE)
    GEN_STEP3(MINUS_ONE, ONE, ZERO)
#undef GEN_STEP3
        }

    }

}
