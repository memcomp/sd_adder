#include "addstep_1.h"

void addstep_1::calc_c_plus(){
	//c_i_+ = a_i_+ or (B_i and not(a_i_-))
	c_plus= a_plus || (B && !a_minus);
}

void addstep_1::calc_z_minus(){ 
	//z_i_- = (a_i_+ or a_i_-) xor B_i
	// a xor b = (a and not(b)) or (not(a) and b)
	z_minus = ((a_plus || a_minus) && !B) || (!(a_plus || a_minus) && B);
	
//	cout << "addstep1_in: a_plus " << a_plus << " a_minus " << a_minus 
//	     << "  B  " << B << endl ;
}
