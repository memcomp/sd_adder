#include "systemc.h"
#include "common.h"
//#include "wordlength.h"



typedef sd_digit sd_word[length];

SC_MODULE(SD_register_array){

      sc_in<bool>      clock ;
/*      sc_bv<length>    adr ;*/
	  // ist dann eine der Register-Zeilen
      sc_in<int>       adr ;
	
      sc_in<sd_word>   sd_word_in  ;	
      sc_out<sd_word>  sd_word_out ;

      sd_word          SD_reg_array[length] ;

      void read() ;
      void write() ;
		
	  //SC_CTOR: Konstruktor
      SC_CTOR(SD_register_array) {
	     SC_METHOD(read);
	     dont_initialize() ;
             sensitive << clock ; 
	     
	     SC_METHOD(write);
	     dont_initialize() ;
             sensitive << clock ; 	     
      };
	
};	
